#!/bin/bash
#
# Test ssh connection speed by uploading and then downloading a 10Mb test
# file (optionally user-specified size)
#
# Usage:
#   ./sftp-speed-test.sh user@hostname [test file size in Mbs]
#

ssh_server=$1
test_file=".sftp-test-file"

# Optional: user specified test file size in Mbs
if test -z "$2"
then
  # default size is 10mB
  test_size="10"
else
  test_size=$2
fi


# generate a file of all zeros
echo "Generating $test_size kB test file..."
`dd if=/dev/zero of=$test_file bs=$(echo "$test_size*1024*1024" | bc) \
  count=1 &> /dev/null`

# upload test
echo "Testing upload to $ssh_server..."
echo "put ${test_file}" | sftp $ssh_server

# download test
echo "Testing download from $ssh_server..."
sftp $ssh_server:$test_file $test_file

# clean up
echo "Removing test file on $ssh_server..."
`ssh $ssh_server "rm $test_file"`
echo "Removing test file locally..."
`rm $test_file`

echo "Test finished"
